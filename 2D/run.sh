#!/bin/bash
mkdir build
cd build
cmake ../
make
./fdtd_2D < ../in2d > ../out
cd ..
head -n 25 out > out0
tail -n 26 out > outT
